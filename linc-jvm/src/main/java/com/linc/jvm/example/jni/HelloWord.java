package com.linc.jvm.example.jni;

public class HelloWord {

    private int i = 10;

    public native static void print();

    public native int show(String s);

    public native static int add(int a, int b);

    public static void main(String[] args) {
//        System.out.println(System.getProperty("java.library.path"));
        System.loadLibrary("jni");

        print();
        //System.out.println(new HelloWord().show("jni"));
        System.out.println(add(1 , 5));
    }
}

