package com.linc.jvm.example.function;

public class Invoke {

    public static void main(String[] args) {
//        invokeStatic();
//        invokeVirtual();
//        invokeSpecial();

//        int i = 10;
//        System.out.println(Other.toHexString(i));

//        Integer ii = new Integer(1);
//        System.out.println(ii);

        String s = new String("linc");
        System.out.println(s);

//        System.out.println(add(1, 2, 3));
//        System.out.println(add(1, 2, 3));

//        new Invoke().add2(1,2, 3);
    }

    public static int add(int a, int b, int c) {
        return a + b + c;
    }

    public void add2(int a, int b, int c) {
        System.out.println(a + b+ c);
    }

    public static void invokeStatic() {
        test();
    }

    public static void invokeVirtual() {
        new Invoke().show();
    }

    public static void invokeSpecial() {
        new Invoke().hello();
    }

    public static void test() {
        System.out.println("test");
    }

    public void show() {
        System.out.println("show");
    }

    private void hello() {
        System.out.println("hello");
    }

//    private void tmp() {
//        new Thread(() -> {
//
//        });
//    }
}

