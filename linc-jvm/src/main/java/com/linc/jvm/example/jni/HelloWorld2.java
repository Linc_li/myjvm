package com.linc.jvm.example.jni;

public class HelloWorld2 {
    // 测试读写静态属性
    public static int val = 10;

    public static native void showVal();
    public static native int changeVal();
    public static native int changeVal(int v);

    // 测试读写非静态属性
    public int val2 = 1000;

    public native void showVal2();
    public native int changeVal2(int v);

    //=============================

    public static native void hi();

    public native void hello();

    public static native int add(int a, int b);

    /**
     * jstring
     * printf char×
     * @param name
     */
    public static native void showName(String name);

    public static native String showName(byte[] name);

    public static native JavaThread createThread();

    public static native void threadRunFast(JavaThread thread);

    public static void main(String[] args) {
//        System.out.println(System.getProperty("java.library.path"));

        System.loadLibrary("jni");

        hi();

//        threadRunFast(new JavaThread());

//        showName("linc");

//        JavaThread thread = createThread();
//        System.out.println(thread);
//
//        thread.run();

//        changeVal(100);
//        new HelloWorld().showVal2();
//        new HelloWorld().changeVal2(100);
    }
}
