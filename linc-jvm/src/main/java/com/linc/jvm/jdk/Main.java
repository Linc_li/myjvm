package com.linc.jvm.jdk;

import com.linc.jvm.hotspot.src.share.vm.classfile.BootClassLoader;
import com.linc.jvm.hotspot.src.share.vm.oops.InstanceKlass;
import com.linc.jvm.hotspot.src.share.vm.oops.MethodInfo;
import com.linc.jvm.hotspot.src.share.vm.prims.JavaNativeInterface;
import com.linc.jvm.hotspot.src.share.vm.runtime.JavaThread;
import com.linc.jvm.hotspot.src.share.vm.runtime.Threads;
import com.linc.jvm.jdk.sun.misc.Unsafe;

public class Main {
    private static String PATH_HELLOWORLD = "com.linc.jvm.example.Helloworld";
    private static String PATH_CHAR3_PRINTBASIC = "com.linc.jvm.example.char3.PrintBasic";
    private static String PATH_CHAR3_PRINTDOUBLE = "com.linc.jvm.example.char3.PrintDouble";
    private static String PATH_OPERATION_ADDADD = "com.linc.jvm.example.operation.AddAdd";
    private static String PATH_CONDITION_IFINT = "com.linc.jvm.example.condition.IfInt";
    public static void main(String[] args) {
        startJVM();
    }

    private static void startJVM(){
        // 通过AppClassLoader加载main函数所在的类
        InstanceKlass mainKlass = BootClassLoader.loadMainKlass(
                PATH_HELLOWORLD);
        // 找到main方法
        MethodInfo mainMethod = JavaNativeInterface.getMethodID(mainKlass,"main", "([Ljava/lang/String;)V");

        // 创建线程
        JavaThread thread = new JavaThread();

        Threads.getThreadList().add(thread);
        Threads.setCurrentThread(thread);

        // 执行main方法
        JavaNativeInterface.callStaticMethod(mainMethod);
    }
}