package com.linc.jvm.jdk.sun.misc;

public final class Unsafe {

    public static native long allocateMemory(long size);

}