package com.linc.jvm.hotspot.src.share.vm.utilities;

import lombok.Data;

/**
 * Created By linc
 */
@Data
public class AccessFlags {

    private int flag;

    public AccessFlags(int flag) {
        this.flag = flag;
    }

    public boolean isStatic() {
        return (flag & BasicType.JVM_ACC_STATIC) != 0;
    }
}
